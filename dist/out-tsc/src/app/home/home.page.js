import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.LOG_TAG = "HomePage-";
        console.log(this.LOG_TAG, ' constructor');
    }
    HomePage.prototype.ngOnChanges = function (changes) {
        console.log(this.LOG_TAG, ' ngOnChanges (Invoked every time there is a change in one of th input properties of the component.)');
        for (var propName in changes) {
            /*let chng = changes[propName];
            let cur  = JSON.stringify(chng.currentValue);
            let prev = JSON.stringify(chng.previousValue);
            this.changeLog.push(`propName: currentValue = cur, previousValue = prev`);*/
        }
    };
    /** lifecycle */
    HomePage.prototype.ngOnInit = function () {
        console.log(this.LOG_TAG, 'ngOnInit (Invoked when given component has been initialized. This hook is only called once!)');
    };
    HomePage.prototype.ngOnDestroy = function () {
        console.log(this.LOG_TAG, 'ngOnDestroy (This method will be invoked just before Angular destroys the component.)');
    };
    /**  Hooks for the components children */
    HomePage.prototype.ngAfterContentInit = function () {
        console.log(this.LOG_TAG, 'ngAfterContentInit (Invoked after Angular performs any content projection into the components view)');
    };
    HomePage.prototype.ngAfterContentChecked = function () {
        // console.log(this.LOG_TAG, 'ngAfterContentChecked Invoked each time the content of the given component has been checked by the change detection mechanism of Angular.');
    };
    HomePage.prototype.ngAfterViewInit = function () {
        console.log(this.LOG_TAG, 'ngAfterViewInit Invoked when the component’s view has been fully initialized');
    };
    HomePage.prototype.ngAfterViewChecked = function () {
        // console.log(this.LOG_TAG, 'ngAfterViewCheckedInvoked each time the view of the given component has been checked by the change detection mechanism of Angular.')
    };
    /** Actions */
    HomePage.prototype.goToRegistration = function () {
        console.log(this.LOG_TAG, 'go to registration navigateForward..');
        this.navCtrl.navigateForward('/registration');
    };
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map