import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import countryJson from "../../assets/country_codes.json";
import { Platform } from 'ionic-angular';
// declare var SMS : any;
var RegistrationPage = /** @class */ (function () {
    function RegistrationPage(navCtrl, androidPermissions, platform) {
        this.navCtrl = navCtrl;
        this.androidPermissions = androidPermissions;
        this.platform = platform;
        this.loadCountries();
        if (this.platform.is("cordova")) {
            this.checkForSMSPermission();
        }
    }
    RegistrationPage.prototype.ngOnInit = function () {
        console.log('RegistrationPage: ngOnInit');
    };
    RegistrationPage.prototype.loadCountries = function () {
        console.log('loadCountries..');
        this.countryArray = new Array();
        for (var index in countryJson.countries) {
            var country = countryJson.countries[index];
            this.countryArray.push(new Country(country.name, country.iso, country.code));
        }
    };
    RegistrationPage.prototype.checkForSMSPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(function (success) { return console.log('Permission granted'); }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.READ_SMS); });
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
    };
    /* emitters **/
    RegistrationPage.prototype.setSelectedCountry = function (country) {
        this.selected_country = country;
        console.log('RegistrationPage: set selected country: ', country);
    };
    /** actions */
    RegistrationPage.prototype.goBack = function () {
        console.log('RegistrationPage: im going back');
        this.navCtrl.pop();
    };
    RegistrationPage.prototype.submit = function () {
        console.log('RegistrationPage: submit button pressed! :', this.selected_country);
    };
    RegistrationPage = tslib_1.__decorate([
        Component({
            selector: 'app-registration',
            templateUrl: './registration.page.html',
            styleUrls: ['./registration.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, AndroidPermissions, Platform])
    ], RegistrationPage);
    return RegistrationPage;
}());
export { RegistrationPage };
var Country = /** @class */ (function () {
    function Country(name, iso, code) {
        this.name = name;
        this.iso = iso;
        this.code = code;
    }
    return Country;
}());
//# sourceMappingURL=registration.page.js.map