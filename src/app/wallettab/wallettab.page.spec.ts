import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallettabPage } from './wallettab.page';

describe('WallettabPage', () => {
  let component: WallettabPage;
  let fixture: ComponentFixture<WallettabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallettabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallettabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
