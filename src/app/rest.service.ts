import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  // just for testing
  apiUrl = 'https://jsonplaceholder.typicode.com';

  constructor(public http: HttpClient) {
    console.log('Hello Rest Service');
  }

  getUsers() {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/users').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  addUser(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/users', JSON.stringify(data))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  /*** For posting data, still, use the same way as previously. 
   If your REST API backend needs additional headers and URL params you can add them like this. */
  
  // this.http.post(this.apiUrl+'/users', JSON.stringify(data), {
  //   headers: new HttpHeaders().set('Authorization', 'my-auth-token'),
  //   params: new HttpParams().set('id', '3'),
  // })
  // .subscribe(res => {
  //   resolve(res);
  // }, (err) => {
  //   reject(err);
  // });
}
