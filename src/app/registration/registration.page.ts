import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { MainPage } from '../main/main.page';
import * as xml2js from "xml2js";
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx'
import countryJson from "../../assets/country_codes.json";
import { Diagnostic } from '@ionic-native/diagnostic/ngx'
import { Platform } from 'ionic-angular';
import { platform } from 'os';
import { RestService } from '../../app/rest.service';


declare var SMS: any;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  selected_country: Country;
  countryArray: Array<Country>;
  users: any;

  constructor(public navCtrl: NavController, private androidPermissions: AndroidPermissions, private diagnostic: Diagnostic, public platform: Platform, public restService: RestService) {

    // just for test //
    console.log("RegistrationPage: ###########");
    console.log(this.platform.is('cordova'));
    console.log(this.platform.is('core'));
    console.log(this.platform.is('ios'));
    console.log(this.platform.is('ipad'));
    console.log(this.platform.is('iphone'));
    console.log(this.platform.is('mobile'));
    console.log(this.platform.is('mobileweb'));
    console.log(this.platform.is('phablet'));
    console.log(this.platform.is('tablet'));
    console.log(this.platform.is('windows'));
    console.log(this.platform.is('electron'));
    console.log(this.platform.is('undefined'));
    console.log("RegistrationPage: ###########");

    this.getUsers();
    this.loadCountries();
    this.checkForSMSPermission();
  }

  ngOnInit() {
    console.log('RegistrationPage: ngOnInit');
  }

  loadCountries() {
    console.log('RegistrationPage: loadCountries..');
    this.countryArray = new Array<Country>();

    for (var index in countryJson.countries) {
      let country = countryJson.countries[index];
      this.countryArray.push(new Country(country.name, country.iso, country.code));
    }
  }

  checkForSMSPermission() {

    this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.RECEIVE_SMS).then((status) => {
      console.log('AuthorizationStatus');
      console.log(status);
      if (status != this.diagnostic.permissionStatus.GRANTED) {
        this.diagnostic.requestRuntimePermission(this.diagnostic.permission.RECEIVE_SMS).then((data) => {
          console.log('getCameraAuthorizationStatus', data);
        })
      } else {
        this.startWatchingForSMS();
        console.log('We have the permission');
      }
    }, (statusError) => {
      console.log('statusError');
      console.log(statusError);
    });
  }

  startWatchingForSMS() {

    SMS.startWatch(() => {
      console.log('RegistrationPage: watching started');
      document.addEventListener('onSMSArrive', (e: any) => {
        var sms = e.data;
        console.log('RegistrationPage: SMS received is:' + sms);
        console.log('RegistrationPage: SMS body:' + sms.body);
        console.log('RegistrationPage: SMS sender:' + sms.address);
      });
    }, Error => {
      console.log('failed to start watching');
    });
  }

  stopWatchingSMS() {

    SMS.stopWatch(() => {
      console.log('RegistrationPage: watching stopped');
    }, function () {
      console.warn('RegistrationPage: failed to stop watching');
    });
  }

  /** http request */
  getUsers() {
    this.restService.getUsers()
    .then(data => {
      this.users = data;
      console.log(this.users);
    });

    // todo add it in html
    // <ion-list inset>
//     <ion-item *ngFor="let user of users">
//     <h2>{{user.name}}</h2>
//     <p>{{user.email}}</p>
//   </ion-item>
// </ion-list>
  }

  /* emitters **/
  setSelectedCountry(country: Country) {
    this.selected_country = country;
    console.log('RegistrationPage: set selected country: ', country);
  }

  onPhoneChanged(value: String) {
    console.log('new value is ' + value);
  }

  /** actions */
  goBack() {
    console.log('RegistrationPage: im going back')
    this.navCtrl.pop();
  }

  submit() {
    console.log('RegistrationPage: submit button pressed! :', this.selected_country);
  }
}

class Country {

  name: String;
  iso: String;
  code: String

  constructor(name, iso, code) {
    this.name = name;
    this.iso = iso;
    this.code = code;
  }
}
