import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  constructor(public navCtrl: NavController) {
    console.log('im in Signup PAGE');
   }

  ngOnInit() {
  }

  goToTabs() {
    this.navCtrl.navigateForward('/tabs');
  }

}
