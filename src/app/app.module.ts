import { ForgotPage } from './forgot/forgot.page';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HomePage } from '../app/home/home.page';
import { RegistrationPage } from '../app/registration/registration.page';
import { MainPage } from '../app/main/main.page';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Platform } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    MainPage,
    ForgotPage],
  entryComponents: [
    AppComponent,
    MainPage,
    ForgotPage
  ],
  imports: [BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule],
  providers: [
    AndroidPermissions,
    Diagnostic,
    Platform,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
