import { Component, ViewChild, SimpleChanges } from '@angular/core';
import { NavigationContainer } from 'ionic-angular/umd/navigation/navigation-container';
import { NavController, NavParams } from '@ionic/angular';
import { RegistrationPage } from '../registration/registration.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  LOG_TAG : String = "HomePage-";

  constructor(public navCtrl: NavController) {
    console.log(this.LOG_TAG ,' constructor')
    setInterval(() => {
      this.goToTabs();
    }, 3000);
  }

  goToTabs() {
    this.navCtrl.navigateForward('/tabs');
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   console.log(this.LOG_TAG ,' ngOnChanges (Invoked every time there is a change in one of th input properties of the component.)');
  //   for (let propName in changes) {
  //     /*let chng = changes[propName];
  //     let cur  = JSON.stringify(chng.currentValue);
  //     let prev = JSON.stringify(chng.previousValue);
  //     this.changeLog.push(`propName: currentValue = cur, previousValue = prev`);*/
  //   }
  // }

  // /** lifecycle */
  // ngOnInit() {
  //   console.log(this.LOG_TAG, 'ngOnInit (Invoked when given component has been initialized. This hook is only called once!)');
  // }

  // ngOnDestroy() {
  //   console.log(this.LOG_TAG, 'ngOnDestroy (This method will be invoked just before Angular destroys the component.)'); 
  // }

  // /**  Hooks for the components children */
  // ngAfterContentInit() {
  //   console.log(this.LOG_TAG, 'ngAfterContentInit (Invoked after Angular performs any content projection into the components view)');
  // }

  // ngAfterContentChecked() {
  //   // console.log(this.LOG_TAG, 'ngAfterContentChecked Invoked each time the content of the given component has been checked by the change detection mechanism of Angular.');
  // }

  // ngAfterViewInit() {
  //   console.log(this.LOG_TAG, 'ngAfterViewInit Invoked when the component’s view has been fully initialized');
  // }

  // ngAfterViewChecked() {
  //   // console.log(this.LOG_TAG, 'ngAfterViewCheckedInvoked each time the view of the given component has been checked by the change detection mechanism of Angular.')
  // }

  // /** Actions */
  // goToRegistration() {
  //   console.log(this.LOG_TAG ,'go to registration navigateForward..');
  //   // this.navCtrl.navigateForward('/registration');
  //   this.navCtrl.navigateForward('/login');
  // }
}
