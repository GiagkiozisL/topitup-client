import { TabsPage } from './tabs.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children:
      [
        {
          path: 'hometab',
          children:
            [
              {
                path: '',
                loadChildren: '../home-tab/home-tab.module#HomeTabPageModule'
              }
            ]
        },
        {
          path: 'wallettab',
          children:
            [
              {
                path: '',
                loadChildren: '../wallettab/wallettab.module#WallettabPageModule'
              }
            ]
        },
        {
          path: '',
          redirectTo: '/tabs/wallettab',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: '/tabs/wallettab',
    pathMatch: 'full'
  }
];

@NgModule({
  imports:[
      RouterModule.forChild(routes)
    ],
  exports:[
      RouterModule
    ]
})
export class TabsPageRoutingModule {}