import { ForgotPage } from './../forgot/forgot.page';
import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  // constructor() {
  //   console.log('im in Login PAGE');
  // }
  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
    console.log('im in Login PAGE');
   }

  ngOnInit() {
  }

  verify() {
    // go to verify page
    // this.navCtrl.push('VerifyPage')
  }

  signup() {
    // go to signup
    // this.navCtrl.push('SignupPage')
    this.navCtrl.navigateForward('/signup');
  }

  async forgot() {
    console.log('forgot clicked');
    const modal = await this.modalCtrl.create({
      component: ForgotPage,
      componentProps: {
        value: 123
      }
    });
    modal.present();
  }

}
